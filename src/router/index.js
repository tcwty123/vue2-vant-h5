import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  { path: '/login', component: () => import('@/views/login') },
  { path: '/register', component: () => import('@/views/register') },
  {
    path: '/',
    redirect: '/article',
    component: () => import('@/views/layout'),
    children: [
      { path: '/collect', component: () => import('@/views/collect') },
      { path: '/article', component: () => import('@/views/article') },
      { path: '/like', component: () => import('@/views/like') },
      { path: '/user', component: () => import('@/views/user') }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
