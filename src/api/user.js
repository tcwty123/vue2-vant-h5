import request from '@/utlis/request'

/**
 * register注册用户接口
 * @param {*} data
 * @returns
 */
export const reqRegister = data => request({
  method: 'post',
  url: '/h5/user/register',
  data
})

/**
 * 登录
 * @param {*} data
 * @returns
 */
export const reqLogin = data => request({
  method: 'post',
  url: '/h5/user/login',
  data
})

/**
 * 获取面经列表
 * @param {*} current
 * @param {*} pageSize
 * @returns
 */
export const reqArticleInfo = (current, pageSize, sorter) => request({
  method: 'get',
  url: '/h5/interview/query',
  params: { current, pageSize, sorter }
})
