import Vue from 'vue'
import { Button, Icon, Tabbar, TabbarItem, Form, Field, Toast, Tab, Tabs, Cell, List } from 'vant'

Vue.use(Tabbar)
  .use(TabbarItem)
  .use(Button)
  .use(Icon)

Vue.use(Form)
Vue.use(Field)

Vue.use(Toast)

Vue.use(Tab)
Vue.use(Tabs)
Vue.use(Cell)
Vue.use(List)
